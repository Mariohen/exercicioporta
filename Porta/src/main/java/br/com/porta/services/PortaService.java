package br.com.porta.services;

import br.com.porta.models.Porta;
import br.com.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criar(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta buscarPortaPorId(int id) {
        Optional<Porta> byId = portaRepository.findById(id);

        if(!byId.isPresent()) {
            throw new PortaNotFoundException();
        }

        return byId.get();

    }

}
