package br.com.porta.controllers;

import br.com.porta.models.Porta;
import br.com.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    public Porta criar(@RequestBody @Valid Porta porta){
        return portaService.criar(porta);
    }

    @GetMapping("/{id}")
    public Porta getById(@PathVariable(name = "id") int id) {
        return portaService.buscarPortaPorId(id);
    }


}
