package br.com.porta.repositories;

import br.com.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository <Porta, Integer> {

}
