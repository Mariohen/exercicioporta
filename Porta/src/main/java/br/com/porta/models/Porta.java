package br.com.porta.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "O nome do andar não pode ser nulo")
    @NotBlank(message = "O nome do andar não pode estar em branco")
    @Column(unique = true)
    private String andar;


    @NotNull(message = "O nome do sala não pode ser nulo")
    @NotBlank(message = "O nome do sala não pode estar em branco")
    @Column(unique = true)
    private String sala;

    public Porta() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }


}
