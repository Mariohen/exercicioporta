package br.com.Consumer;

import br.com.acesso.DTOs.AcessoKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import com.opencsv.CSVWriter;


@Component
public class AcessoConsumerKafka {

    @KafkaListener(topics = "spec4-mario-henrique-2", groupId = "Mario")
    public void receber(@Payload AcessoKafka acessoKafka) throws IOException {
        System.out.println(acessoKafka.getClienteId() + " " + acessoKafka.getPortaId() + " " + acessoKafka.getValidacaoDeAcesso());

        String[] cabecalho = {"ClienteID", "PortaId", "ValidarAcesso"};

        List<String[]> linhas = new ArrayList<>();
        linhas.add(new String[]{Integer.toString(acessoKafka.getClienteId()),
                                Integer.toString(acessoKafka.getPortaId()),
                                Boolean.toString(acessoKafka.getValidacaoDeAcesso())});


        Writer writer = Files.newBufferedWriter(Paths.get("acesso.csv"));
        CSVWriter csvWriter = new CSVWriter(writer);

        csvWriter.writeNext(cabecalho);
        csvWriter.writeAll(linhas);

        csvWriter.flush();
        writer.close();

    }


}
