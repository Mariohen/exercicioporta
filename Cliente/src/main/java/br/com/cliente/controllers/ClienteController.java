package br.com.cliente.controllers;

import br.com.cliente.models.Cliente;
import br.com.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    public ClienteService clienteService;

    @PostMapping
    public Cliente criar(@RequestBody @Valid Cliente cliente){
        return clienteService.criar(cliente);
    }

    @GetMapping("/{id}")
    public Cliente getById(@PathVariable(name = "id") int id) {
        return clienteService.buscarClientePorId(id);
    }

}
