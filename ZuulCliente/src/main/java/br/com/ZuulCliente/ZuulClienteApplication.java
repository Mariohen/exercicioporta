package br.com.ZuulCliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZuulClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulClienteApplication.class, args);
	}

}
