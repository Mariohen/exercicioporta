package br.com.acesso.Controllers;

import br.com.acesso.Cliente.ClienteAcesso;
import br.com.acesso.Models.Acesso;
import br.com.acesso.Porta.PortaAcesso;
import br.com.acesso.Services.AcessoProducerKafka;
import br.com.acesso.Services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

     @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso criar(@RequestBody Acesso acesso){

   Acesso objetoAcesso = acessoService.criar(acesso);
        return acesso;
    }

    @GetMapping("/{portaId}/{clienteId}")
    public Acesso buscarPorNumero(@PathVariable(name = "portaId") Integer portaId, @PathVariable(name = "clienteId") Integer clienteId){
        return acessoService.buscarCartaoPeloId(portaId, clienteId);
    }


    @DeleteMapping("/{portaId}/{clienteId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleterAcesso(@PathVariable(name = "portaId") Integer portaId, @PathVariable(name = "clienteId") Integer clienteId){
        acessoService.deletarAcesso(portaId, clienteId);
    }

}
