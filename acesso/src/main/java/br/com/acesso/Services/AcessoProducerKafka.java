package br.com.acesso.Services;

import br.com.acesso.DTOs.AcessoKafka;
import br.com.acesso.Models.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducerKafka {

    @Autowired
    private KafkaTemplate<String, AcessoKafka> producer;

    public void enviarAoKafka(AcessoKafka acessoKafka) {
        producer.send("spec4-mario-henrique-2", acessoKafka);
    }


}
