package br.com.acesso.Services;

import br.com.acesso.Cliente.ClienteAcesso;
import br.com.acesso.DTOs.AcessoKafka;
import br.com.acesso.Models.Acesso;
import br.com.acesso.Porta.PortaAcesso;
import br.com.acesso.Repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteAcesso clienteAcesso;

    @Autowired
    private PortaAcesso portaAcesso;

    @Autowired
    private AcessoProducerKafka acessoProducerKafka;


    public Acesso criar(Acesso acesso) {
        Acesso objetoAcesso = acessoRepository.save(acesso);
        return acesso;
    }

    public Acesso buscarCartaoPeloId(int portaId, int clienteId) {
        Acesso acesso = acessoRepository.findByPortaIdAndClienteId(portaId, clienteId);

        AcessoKafka acessoKafka = new AcessoKafka();
        acessoKafka.setClienteId(acesso.getClienteId());
        acessoKafka.setPortaId(acesso.getPortaId());

        if (acesso != null) {
            acessoKafka.setValidacaoDeAcesso(true);
        } else {
            acessoKafka.setValidacaoDeAcesso(false);
        }
        acessoProducerKafka.enviarAoKafka(acessoKafka);
        System.out.println(acessoKafka.getClienteId() + " " + acessoKafka.getValidacaoDeAcesso());

        if (acesso != null) {
            return acesso;
        } else {
            throw new AcessoNotFoundException();
        }

    }

    public void deletarAcesso(int portaId, int clienteId) {
        Acesso acesso = acessoRepository.findByPortaIdAndClienteId(portaId, clienteId);

        if (acesso != null) {
            acessoRepository.deleteById(acesso.getId());
        } else {
            throw new AcessoNotFoundException();
        }
    }
}