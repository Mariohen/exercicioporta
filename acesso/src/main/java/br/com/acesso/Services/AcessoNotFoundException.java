package br.com.acesso.Services;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Chave de acesso não encontrado!!!")
public class AcessoNotFoundException extends RuntimeException {
}