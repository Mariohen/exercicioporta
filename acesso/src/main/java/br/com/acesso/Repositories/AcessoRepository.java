package br.com.acesso.Repositories;

import br.com.acesso.Models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository <Acesso, Integer> {
    Acesso findByPortaIdAndClienteId(int portaId, int clienteId);

}
