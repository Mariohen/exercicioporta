package br.com.acesso.Porta;

import br.com.acesso.Cliente.Cliente;
import br.com.acesso.Cliente.ClienteAcessoConfiguracao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaAcessoConfiguracao.class)
public interface PortaAcesso {

    @GetMapping("/porta/{id}")
    Porta getById(@PathVariable Integer id);

}
