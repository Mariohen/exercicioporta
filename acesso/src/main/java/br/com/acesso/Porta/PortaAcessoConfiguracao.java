package br.com.acesso.Porta;

import br.com.acesso.Cliente.ClienteAcessoConfiguracao;
import br.com.acesso.Cliente.ClienteAcessoFallBack;
import br.com.acesso.Cliente.ClienteAcessoLoadBalanceConfiguracao;
import br.com.acesso.Cliente.ClienteDecoder;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;

public class PortaAcessoConfiguracao {

    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new ClienteDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new PortaAcessoFallBack(), RetryableException.class)
                .withFallbackFactory(PortaAcessoLoadBalanceConfiguracao::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
