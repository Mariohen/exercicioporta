package br.com.acesso.Porta;

import br.com.acesso.Cliente.Cliente;

public class PortaAcessoFallBack implements PortaAcesso{

    @Override
    public Porta getById(Integer id) {
        Porta porta = new Porta();
        porta.setId(99999);
        porta.setAndar("Andar não encontrado");
        porta.setSala("Sala não encontrada");

        return porta;
    }

}
