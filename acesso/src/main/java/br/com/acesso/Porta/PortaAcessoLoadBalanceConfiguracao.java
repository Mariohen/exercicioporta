package br.com.acesso.Porta;

import br.com.acesso.Cliente.Cliente;
import br.com.acesso.Services.ClienteNotFoundException;
import br.com.acesso.Services.PortaNotFoundException;

public class PortaAcessoLoadBalanceConfiguracao implements PortaAcesso{

    private Exception exception;

    public PortaAcessoLoadBalanceConfiguracao(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Porta getById(Integer id) {
        if(exception.getCause() instanceof PortaNotFoundException) {
            Porta porta = new Porta();
            return porta;
        }
        throw (RuntimeException) exception;
    }

}
