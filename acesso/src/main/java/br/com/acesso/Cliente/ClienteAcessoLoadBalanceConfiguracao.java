package br.com.acesso.Cliente;

import br.com.acesso.Services.ClienteNotFoundException;

public class ClienteAcessoLoadBalanceConfiguracao implements ClienteAcesso{

    private Exception exception;

    public ClienteAcessoLoadBalanceConfiguracao(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Cliente getById(Integer id) {
        if(exception.getCause() instanceof ClienteNotFoundException) {
            Cliente cliente = new Cliente();
            return cliente;
        }
        throw (RuntimeException) exception;
    }

}
