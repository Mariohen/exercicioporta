package br.com.acesso.Cliente;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteAcessoConfiguracao {
    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new ClienteDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClienteAcessoFallBack(), RetryableException.class)
                .withFallbackFactory(ClienteAcessoLoadBalanceConfiguracao::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}

