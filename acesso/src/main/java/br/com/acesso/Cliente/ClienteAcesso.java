package br.com.acesso.Cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteAcessoConfiguracao.class)
public interface ClienteAcesso {

    @GetMapping("/cliente/{id}")
    Cliente getById(@PathVariable Integer id);
}
