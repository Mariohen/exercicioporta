package br.com.acesso.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AcessoKafka {

    private Integer portaId;
    private Integer clienteId;
    private Boolean validacaoDeAcesso;

    public AcessoKafka() {}

    public Integer getPortaId() {
        return portaId;
    }

    public void setPortaId(Integer portaId) {
        this.portaId = portaId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getValidacaoDeAcesso() {
        return validacaoDeAcesso;
    }

    public void setValidacaoDeAcesso(Boolean validacaoDeAcesso) {
        this.validacaoDeAcesso = validacaoDeAcesso;
    }
}
